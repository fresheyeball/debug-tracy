module Main where

import           Debug.Tracy

main :: IO ()
main = print $ unScope $ do
  x <- scope "x" $ [0..(3 :: Int)]
  y <- scope "y" ([] :: [Int]) -- return (4 :: Int)
  return (x, y)



--   x <- scope "x" $ Just (3 :: Int)
--   y <- scope "y" $ Just (5 :: Int) -- (Nothing :: Maybe Int)
--   return (tracy "x" x, keanu y)
