# Debug Tracy

[![pipeline status](https://gitlab.com/fresheyeball/debug-tracy/badges/master/pipeline.svg)](https://gitlab.com/fresheyeball/debug-tracy/commits/master)

![](https://d1466nnw0ex81e.cloudfront.net/n_iv/600/955055.jpg)

Sloppy debug tools, (that get the job done), I was sick of writing inline. Now for your enjoyment. Go investigate.
